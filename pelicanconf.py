#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Levente'
SITENAME = u'testPYLVAX'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
DISPLAY_PAGES_ON_MENU=True

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
            ('Google', 'http://google.com'),)

# Social widget
SOCIAL = (('Pylvax FaceBook', 'https://www.facebook.com/pylvax'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

### Output redirect to another repo
OUTPUT_PATH = '../pylvax.bitbucket.org/'

### Delete unnecessary files
DELETE_OUTPUT_DIRECTORY = True
OUTPUT_RETENTION = [".git"]
